import '../sass/main.sass'

import HomeHanlder from './components/HomeHanlder';
import FormHandler from './components/FormHandler';

const homeHandler = new HomeHanlder();
const formHandler = new FormHandler();
