import axios from 'axios';

export default class FormHandler {
  constructor(url = 'http://localhost:3000/api/v1/players',
              id = window.location.pathname.substring(window.location.pathname.lastIndexOf('/') + 1),
              wrapperCreate = 'form--add',
              wrapperUpdate = 'form--update',
              form = '.form',
              uploadBtn = '#file',
              uploadLabel = '.form__label--file',
              nameField = '.form__input[name="name"]',
              ratingField = '.form__input[name="rating"]',
              biographyField = '.form__input[name="biography"]',
              fileField = '.form__file[name="imagePath"]') {
    this.url = url;
    this.id = id;
    this.wrapperCreate = wrapperCreate;
    this.wrapperUpdate = wrapperUpdate;
    this.form = form;
    this.uploadBtn = uploadBtn;
    this.uploadLabel = uploadLabel;
    this.nameField = nameField;
    this.ratingField = ratingField;
    this.biographyField = biographyField;
    this.fileField = fileField;
    this.init();
  }

  createPlayer(e, url = this.url) {
    e.preventDefault();
    const player = this.getData();
    const config = {
      headers: {'content-type': 'multipart/form-data'}
    };
    axios.post(url, player, config)
      .then(() => window.location.pathname = '/')
      .catch(err => {
        document.querySelector('.form__error').innerHTML = err.response.data.error;
      })
  }

  updatePlayer(e, url = this.url, id = this.id) {
    e.preventDefault();
    const player = this.getData();
    axios.put(`${url}/${id}`, player)
      .then(() => window.location.pathname = '/')
      .catch(err => {
        document.querySelector('.form__error').innerHTML = err.response.data.error;
      })
  }

  changeLabel(e, uploadLabel = this.uploadLabel) {
    const fileName = e.target.files[0] ? e.target.files[0].name : null;
    if (fileName) {
      document.querySelector(uploadLabel).innerHTML = fileName;
    } else {
      document.querySelector(uploadLabel).innerHTML = 'Upload file'
    }
  }

  getData() {
    const name = document.querySelector(this.nameField).value;
    const rating = document.querySelector(this.ratingField).value;
    const biography = document.querySelector(this.biographyField).value;
    const filePath = document.querySelector(this.fileField).files[0];
    const fd = new FormData();
    fd.append('name', name);
    fd.append('rating', rating);
    fd.append('biography', biography);
    if (filePath) {
      fd.append('imagePath', filePath);
    }
    return fd;
  }

  getPlayerInfo(url = this.url, id = this.id) {
    axios.get(`${url}/${id}`)
      .then(response => this.fillForm(response))
      .catch(() => alert('Something gone wrong, please try later'))
  }

  fillForm(data) {
    const {name, rating, biography} = data.data.player;
    document.querySelector(this.nameField).value = name;
    document.querySelector(this.ratingField).value = rating;
    document.querySelector(this.biographyField).value = biography;
  }

  isUpdateForm(form = this.form, wrapperUpdate = this.wrapperUpdate) {
    const formElement = document.querySelector(form);
    return formElement ? formElement.classList.contains(wrapperUpdate) : false;
  }

  addEventListeners(uploadBtn = this.uploadBtn, form = this.form) {
    document.querySelector(uploadBtn).addEventListener('change', e => this.changeLabel(e));
    if (this.isUpdateForm()) {
      document.querySelector(form).addEventListener('submit', e => this.updatePlayer(e));
    } else {
      document.querySelector(form).addEventListener('submit', e => this.createPlayer(e));

    }
  }

  init(form = this.form) {
    if (document.querySelector(form)) {
      this.addEventListeners();

      if (this.isUpdateForm()) {
        this.getPlayerInfo();
      }

    }
  }
}
