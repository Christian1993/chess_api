import axios from 'axios';

export default class HomeHanlder {
  constructor(
    url = 'http://localhost:3000/api/v1/players',
    wrapper = '#players__wrapper',
    playersCountWrapper = '.introduction__count',
    deleteBtn = 'player-teaser__delete',
  ) {
    this.url = url;
    this.wrapper = wrapper;
    this.playersCountWrapper = playersCountWrapper;
    this.deleteBtn = deleteBtn;
    this.init();
  }

  getAll() {
    axios.get(this.url)
      .then(
        response => {
          response.data.players.forEach(item => this.loadContent(item));
          this.setPlayersNumber(response.data.count);
        }
      )
      .catch(error => alert(error))
  }

  loadContent(data, wrapper = this.wrapper) {
    const item = `
      <div class="col-sm-6 col-md-4">
        <div class="player-teaser">
          <div class="player-teaser__image" style="background-image: url('/${data.imagePath}')">
          <span class="player-teaser__delete icon-cancel" data-id="${data._id}"></span>
          <a class="player-teaser__update icon-pencil" href="/update/${data._id}"></a>
          </div>
          <div class="player-teaser__content">
              <p class="player-teaser__name">${data.name}</p>
              <p class="player-teaser__rating">Best rating: ${data.rating}</p>
              <p class="player-teaser__biography">${data.biography}</p>
          </div>
       </div>
     </div>
     `;
    document.querySelector(wrapper).insertAdjacentHTML('afterbegin', item);
  }

  setPlayersNumber(number, playersCountWrapper = this.playersCountWrapper) {
    document.querySelector(playersCountWrapper).innerHTML += number;
  }

  deletePlayer(id, url = this.url) {
    axios.delete(`${url}/${id}`)
      .then(location.reload())
      .catch(error => alert(error))
  }

  addEventListeners(wrapper = this.wrapper, deleteBtn = this.deleteBtn) {
    document.querySelector(wrapper).addEventListener('click', e => {
      if (e.target.classList.contains(deleteBtn)) {
        this.deletePlayer(e.target.dataset.id);
      }
    })
  }

  init() {
    if (document.querySelector(this.wrapper)) {
      this.getAll();
      this.addEventListeners();
    }
  }
}
