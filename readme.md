# Chess API

#### Description:
Node application provides API, that contains data about chess players. It enables CRUD operations (also on images). User interface was implemented with the use of pug and sass. 

#### Technologies used:
* node, 
* mongoDB, 
* javascript, 
* sass,
* pug,
* axios,
* express,
* multer

#### How to run:
1. Download repo
2. In main directory type: _npm_ _install_
3. Start mongoDB
4. Type: _npm_ _run_ _start_
5. One can restore initial database state by typing _npm_ _run_ _seed_

<img src="https://drive.google.com/uc?export=view&id=1zPGn3VgUBYnibPJN5oj8JCZxyupH9OxW" width="500" height="auto">
