const path = require('path');
// const CleanWebpackPlugin = require('clean-webpack-plugin');
// const HtmlWebpackPlugin = require('html-webpack-plugin');
// const FaviconsWebpackPlugin = require('favicons-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin')
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const SassLintPlugin = require('sass-lint-webpack');
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');

module.exports = {
  entry: ['@babel/polyfill', './src/js/app.js'],
  output: {
    path: path.resolve(__dirname, 'public'),
    filename: 'bundle.js'
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: {
          loader: 'babel-loader',
          options: {
            presets: ['@babel/preset-env']
          }
        }
      },
      {
        test: /\.(woff(2)?|ttf|eot|svg)$/,
        use: [
          {
            loader: 'file-loader',
            options: {
              name: '[name].[ext]',
              outputPath: 'fonts/',
            }
          }
        ]
      },
      {
        test: /\.(png|jpg|gif)$/,
        use: [
          {
            loader: 'file-loader',
            options: {
              name: '[name].[ext]',
              outputPath: 'images/',
            }
          }
        ]
      },
      {
        test: /\.s?[ac]ss$/,
        use: [
          MiniCssExtractPlugin.loader,
          {loader: 'css-loader', options: {}},
          {loader: 'postcss-loader', options: {sourceMap: true, plugins: [require('autoprefixer')]}},
          {loader: 'sass-loader', options: {}}
        ]
      },
      // {
      //   test: /\.html$/,
      //   use: ['html-loader']
      // },
    ]
  },
  devtool: 'source-map',
  plugins: [
    new CopyWebpackPlugin([
      {from:'src/images',to:'images'}
    ]),
    new MiniCssExtractPlugin({
      filename: 'main.css',
    }),
    new SassLintPlugin({
      configFile: '/sass-lint.yml',
    }),
    // new HtmlWebpackPlugin({
    //   template: 'src/index.html'
    // }),
    new UglifyJsPlugin({
      cache: true,
      parallel: true,
      uglifyOptions: {
        compress: true,
        ecma: 6,
        mangle: true,
      },
      sourceMap: true,
    }),
    // new CleanWebpackPlugin(path.join(__dirname, 'public')),
    // new FaviconsWebpackPlugin(path.join(__dirname, 'src/img/favicon.png'))
  ],
  // devServer: {
  //   contentBase: path.join(__dirname, 'public'),
  //   compress: true,
  //   port: 3000
  // },
};
