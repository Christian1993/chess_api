module.exports.not_found = (req, res, next) => {
  const error = new Error('Not found');
  next(error);
};

module.exports.error_handler = (error, req, res, next) => {
  const status = error.status;
  if (status === 404) {
    res.status(404).json({message: error.message})
  } else if (status === 400) {
    res.status(400).json({message: error.message})
  } else {
    res.status(500).json({message: error.message})
  }
};
