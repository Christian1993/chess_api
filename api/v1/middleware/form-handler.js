const multer = require('multer');

const storage = multer.diskStorage({
  destination(req, file, cb) {
    cb(null, './public//upload/');
  },
  filename(req, file, cb){
    cb(null, Date.now().toString() + '-' + file.originalname)
  }
});

const fileFilter = (req, file, cb) => {
  if (file.mimetype === 'image/jpeg' || file.mimetype === 'image/png') {
    cb(null, true);
  } else {
    const error = new Error('Only jpg or png extention allowed');
    error.status = 400;
    cb(error, false);
  }
};

const upload = multer({
  fileFilter,
  storage,
  limits: {
    fileSize: 1024 * 1024 * 2
  }
});

module.exports = upload;
