const Player = require('../models/player');
const fse = require('fs-extra');
const mongoose = require('mongoose');

mongoose.connect('mongodb://localhost/chess_players', {useNewUrlParser: true});

const players = [
  {
    name: 'Magnus Carlsen',
    rating: 2882,
    biography: 'Carlsen is a Norwegian chess grandmaster and the current World Chess Champion',
    imagePath: 'public/upload/carlsen_magnus.jpg'
  },
  {
    name: 'Kasparov Garry',
    rating: 2851,
    biography: 'Is a Russian-Armenian chess grandmaster, former world chess champion, writer, and political activist, whom many consider to be the greatest chess player of all time',
    imagePath: 'public/upload/kasparov_garry.jpg'
  },
  {
    name: 'Fabiano Caruana',
    rating: 2844,
    biography: 'Is an Italian-American chess grandmaster. A chess prodigy, he became a grandmaster at the age of 14 years, 11 months and 20 days—the youngest grandmaster in the history of both Italy and the United States at the time',
    imagePath: 'public/upload/caruana_fabiano.jpg'
  },
  {
    name: 'Hikaru Nakamura',
    rating: 2816,
    biography: 'He is a four-time United States Chess Champion, who won the 2011 edition of Tata Steel Group A and represented the United States at five Chess Olympiads, winning a team gold medal and two team bronze medals',
    imagePath: 'public/upload/nakamura_hikaru.jpg'
  },
  {
    name: 'Vassily Ivanchuk',
    rating: 2787,
    biography: 'Is a Ukrainian chess grandmaster and a former World Rapid Chess Champion. Ivanchuk has won Linares, Wijk aan Zee, Tal Memorial, Gibraltar Masters and M-Tel Masters titles',
    imagePath: 'public/upload/ivanchuk_vasyll.jpg'
  },
];

players.forEach(player => {
  Player.create(player);
});

fse.copy('./seed_images', '../../../public/upload', err => {
  if (err) {
    console.error(err);
  } else {
    console.log('success!');
  }
});
