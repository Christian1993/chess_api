const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const PlayerSchema = new Schema({
  name: {
    type: String,
    required: [true, 'Name is required']
  },
  rating: {
    type: Number,
    required: [true, 'Rating is required']
  },
  biography: {
    type: String,
    required: [true, 'Biography is required'],
    maxlength: [300, 'Biography must be no longer than 300 characters'],
  },
  imagePath: {
    type: String,
    default: '/uploads/hoody.jpg'
  }
});

const Player = mongoose.model('player', PlayerSchema);
module.exports = Player;