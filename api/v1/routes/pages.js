const express = require('express');
const pagesController = require('../controllers/pages');
const router = express.Router();

router.get('/', pagesController.index);
router.get('/add', pagesController.add);
router.get('/update/:id', pagesController.update);

module.exports = router;
