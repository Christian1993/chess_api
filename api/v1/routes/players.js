const express = require('express');
const playersController = require('../controllers/players');
const upload = require('../middleware/form-handler');

const router = express.Router();

router.post('/', upload.single('imagePath'), playersController.create);
router.get('/', playersController.read_all);
router.get('/:playerId', playersController.read_one);
router.put('/:playerId', upload.single('imagePath'), playersController.update);
router.delete('/:playerId', playersController.delete);

module.exports = router;
