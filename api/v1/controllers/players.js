const Player = require('../models/player');
const fs = require('fs');

module.exports.create = (req, res) => {
  console.log(req.body);
  let filePath = req.file ? req.file.path : undefined;
  if (filePath) {
    filePath = filePath.replace(/\\/g, '/');
  }
  Player.create({
    name: req.body.name,
    rating: req.body.rating,
    biography: req.body.biography,
    imagePath: filePath
  })
    .then(player => {
      Player.findOne({_id: player._id})
        .select('name _id rating biography imagePath')
        .then(player => {
          res.status(201).json({player})
        })
    })
    .catch(err => {
      res.status(400).json({'error': err.message})
    })
};

module.exports.read_all = (req, res) => {
  Player.find()
    .select('name _id rating biography imagePath')
    .then(players => {
      res.status(200).json({
        count: players.length,
        players
      })
    })
    .catch(err => {
      res.status(500).json({'error': err.message})
    })
};

module.exports.read_one = (req, res) => {
  Player.findOne({_id: req.params.playerId})
    .select('name _id rating biography imagePath')
    .then(player => {
      if (player) {
        res.status(200).json({player});
      } else {
        res.status(404).json({})
      }
    })
    .catch(err => {
      res.status(500).json({'error': err.message})
    })
};
// TODO: refactor this
module.exports.update = (req, res) => {
  const filePath = req.file ? req.file.path : null;
  const playerObj = {
    ...req.body
  };
  if (filePath && filePath !== '/uploads/hoody.jpg') {
    playerObj['imagePath'] = filePath;
  }
  Player.findOne({_id: req.params.playerId})
    .select('name _id rating biography imagePath')
    .then(player => {
      if (filePath) {
        fs.unlinkSync(player.imagePath);
      }
      player.set(playerObj);
      player.save((err, player) => {
        if (err) {
          res.status(500).json({'error': err.message})
        } else {
          res.status(200).json({player});
        }
      })
    })
    .catch(err => {
      res.status(500).json({'error': err.message})
    })
};

module.exports.delete = (req, res) => {
  Player.findByIdAndDelete({_id: req.params.playerId})
    .select('name _id rating biography imagePath')
    .then(player => {
      if (player) {
        res.status(200).json({player});
        const imagePath = player.imagePath;
        if (imagePath !== '/public/uploads/hoody.jpg') {
          fs.unlinkSync(imagePath);
        }
      } else {
        res.status(404).json({})
      }
    })
    .catch(err => {
      res.status(500).json({'error': err.message})
    })
};
