module.exports.index = (req, res) => {
  res.render('index');
};

module.exports.add = (req, res) => {
  res.render('add');
};

module.exports.update = (req, res) => {
  res.render('update', {id: req.params.id});
};
