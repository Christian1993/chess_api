const express = require('express');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const playersRouter = require('./api/v1/routes/players');
const pagesRouter = require('./api/v1/routes/pages');
const errorHandler = require('./api/v1/middleware/error-handler');
const corsHandler = require('./api/v1/middleware/cors-handler');
const app = express();

mongoose.set('useFindAndModify', false);
mongoose.connect('mongodb://localhost/chess_players', {useNewUrlParser: true});

app.set('view engine', 'pug');
app.set('views', __dirname + '/views');
app.use('/public', express.static('public'));
app.use(bodyParser.json());

app.use(corsHandler.cors_handler);
app.use(pagesRouter);
app.use('/api/v1/players', playersRouter);

// TODO: add 404 page
app.use(errorHandler.not_found);
app.use(errorHandler.error_handler);

module.exports = app;
